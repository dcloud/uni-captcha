const { builtinModules } = require('module')
const { terser } = require('rollup-plugin-terser')
const resolve = require('@rollup/plugin-node-resolve')
const commonjs = require('@rollup/plugin-commonjs')
const copy = require('rollup-plugin-copy')
const json = require('@rollup/plugin-json')

const plugins = [resolve(), commonjs(), json()]
if (process.env.NODE_ENV === 'production') {
  plugins.push(
    terser({
      output: {
        comments: false
      }
    })
  )
}

module.exports = function (moduleName) {
  return {
    input: 'src/index.js',
    output: {
      file: 'dist/index.js',
      format: 'commonjs',
      exports: 'auto'
    },
    plugins: [
      ...plugins,
      copy({
        targets: [
          {
            src: 'LICENSE',
            dest: 'dist',
            rename: 'LICENSE.md'
          },
          {
            src: 'src/assets/fonts',
            dest: 'dist'
          }
        ]
      })],
    external: [
      ...builtinModules
    ]
  }
}
