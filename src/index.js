import {
  wrapFn
} from './share/index'
import { Captcha, VerifyCode } from './lib/index'

const verifyCode = new VerifyCode()

Object.keys(verifyCode).forEach(methodName => {
  Captcha.prototype[methodName] = wrapFn(verifyCode[methodName])
})

const uniCaptchaOrigin = new Captcha()
const uniCaptcha = new Proxy(uniCaptchaOrigin, {
  get (target, prop) {
    if (prop in target) {
      if (typeof target[prop] === 'function') {
        return wrapFn(target[prop]).bind(uniCaptcha)
      } else {
        return target[prop]
      }
    }
  }
})

export default uniCaptcha
