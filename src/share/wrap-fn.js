import {
  isPlainObject
} from './utils'
import { ErrorCode } from './error'

const asyncFn = async function () { }

export function wrapFn (fn) {
  if (asyncFn.constructor === fn.constructor) {
    return async function () {
      const res = await fn.apply(this, arguments)
      if (isPlainObject(res)) {
        if (res.msg) {
          res.message = res.msg
          res.errMsg = res.msg
        }
        if (res.code === 0) {
          res.errCode = res.code
        } else {
          res.errCode = ErrorCode[res.code] || res.code
        }
      }
      return res
    }
  }
  return function () {
    const res = fn.apply(this, arguments)
    if (isPlainObject(res)) {
      if (res.msg) {
        res.message = res.msg
        res.errMsg = res.msg
      }
      if (res.code === 0) {
        res.errCode = res.code
      } else {
        res.errCode = ErrorCode[res.code] || res.code
      }
    }
    return res
  }
}
