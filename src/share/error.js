export class UniCaptchaError extends Error {
  constructor (options) {
    super(options.message)
    this.errMsg = options.message || ''
    Object.defineProperties(this, {
      message: {
        get () {
          return `errCode: ${options.code || ''} | errMsg: ` + this.errMsg
        },
        set (msg) {
          this.errMsg = msg
        }
      }
    })
  }
}

export const ErrorCode = {
  10001: 'uni-captcha-create-fail',
  10002: 'uni-captcha-verify-fail',
  10003: 'uni-captcha-refresh-fail',
  10101: 'uni-captcha-deviceId-required',
  10102: 'uni-captcha-text-required',
  10103: 'uni-captcha-verify-overdue',
  10104: 'uni-captcha-verify-fail',
  50403: 'uni-captcha-interior-fail'
}
