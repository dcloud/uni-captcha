import svgCaptcha from 'svg-captcha-fixed'
import BmpCaptcha from '../lib/captcha-bmp/captcha.js'

// default options
// width: 150,
// height: 50,
// noise: 4,
// color: false,
// background: '#FFFAE8',
// size: 4,
// ignoreChars: '',
// fontSize: 40
// text: '' // 可以传入想要使用的字符

// 制造验证码图片
export function makeCaptcha (options = {}) {
  const { uniPlatform = '', mode = 'svg' } = options

  if (mode === 'svg') {
    let captcha
    if (options.mathExpr) {
      captcha = svgCaptcha.createMathExpr(options)
    } else {
      captcha = svgCaptcha.create(options)
    }
    let base64 = 'data:image/svg+xml;utf8,' + captcha.data.replace(/#/g, '%23')

    if (!uniPlatform || ['mp-toutiao', 'h5', 'web', 'app', 'app-plus'].indexOf(uniPlatform) > -1) {
      base64 = base64.replace(/"/g, '\'').replace(/</g, '%3C').replace(/>/g, '%3E')
    }

    return {
      text: captcha.text,
      base64
    }
  } else {
    // 初始化验证码实例
    const bmpCaptcha = new BmpCaptcha(JSON.parse(JSON.stringify({
      ...options,
      textLength: options.size ? Number(options.size) : undefined,
      textColor: hexToRgb(options.color),
      background: hexToRgb(options.background)
    })))

    // 绘制验证码，得到验证码text和buffer
    const { text, base64 } = bmpCaptcha.draw({
      text: options.text
    })

    return {
      text,
      base64
    }
  }
}

function hexToRgb (hex) {
  if (!hex) return
  // 如果 hex 参数是一个数组，直接返回
  if (Array.isArray(hex)) {
    return hex
  }
  // 如果 hex 不是一个有效的十六进制颜色代码，返回 null 或其他适当的值
  if (!/^[0-9A-Fa-f]{6}$/.test(hex)) {
    return
  }
  // 去掉前面的 # 号（如果有）
  hex = hex.replace(/^#/, '')
  // 将十六进制颜色代码拆分为 R、G、B 部分
  const rHex = hex.substring(0, 2)
  const gHex = hex.substring(2, 4)
  const bHex = hex.substring(4, 6)

  // 将十六进制转换为十进制
  const r = parseInt(rHex, 16)
  const g = parseInt(gHex, 16)
  const b = parseInt(bHex, 16)

  // 返回 RGB 格式的数组
  return [r, g, b]
}
