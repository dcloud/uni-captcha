const db = uniCloud.database()
const verifyCollectionName = 'opendb-verify-codes'
const verifyCollection = db.collection(verifyCollectionName)

export {
  verifyCollection,
  verifyCollectionName,
  db
}
